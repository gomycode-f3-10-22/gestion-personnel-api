const fonctionModel = require("../models/fonction")

const create = async (req, res) =>{
    const body = new fonctionModel(req.body);
    const data = await body.save();
    res.status(200).json(data)
}

const all = async (req, res)=>{

    const data = await fonctionModel.find();
    res.status(200).json(data)
}

const one = async (req, res)=>{

    const data = await fonctionModel.findOne({_id: req.params._id});
    res.status(200).json(data)
}

const remove = async (req, res)=>{

    const data = await fonctionModel.findOneAndDelete({_id: req.params._id});
    res.status(200).json(data)
}

const update = async (req, res)=>{

    const data = await fonctionModel.findOneAndUpdate({_id: req.params._id}, req.body);
    res.status(200).json(data)
}

module.exports = {create, all, one, remove, update}