const express = require('express');
const router = express.Router();
const fonctionController = require('../controllers/fonction');

router.post("", fonctionController.create)
router.get("", fonctionController.all)
router.get("/:_id", fonctionController.one)
router.delete("/:_id", fonctionController.remove)
router.put("/:_id", fonctionController.update)


module.exports = router;